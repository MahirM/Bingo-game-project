package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Mahir on 19.09.2017..
 */
public class ResultBoard extends Pane {
    private CountdownTimer countdownTimer;
    private Label ticker;
    private Button start;
    private Button reset;
    private ResultBoard resultBoard;
    private NumberWithBalloon numberWithBalloon;
    private ArrayList<NumberWithBalloon> numberWithBalloonArrayList = new ArrayList<>();
    private ArrayList<String> tickerList = new ArrayList<>();
    private NumbersBoard numbersBoard;
    private Ticket ticket;
    private boolean isStarted = false;
    private MyDialog alert;


    public ResultBoard() {
        setStyle("-fx-background-color: #4DD0E1");
        setPrefSize(600, 400);
        setLayoutX(0);
        setLayoutY(0);
        createBoard();
        setTicker();
    }

    public void createBoard() {
       getChildren().removeAll(numberWithBalloonArrayList);
        getChildren().removeAll(reset);
        getChildren().removeAll(ticker);
        getChildren().removeAll(start);
        numberWithBalloonArrayList.clear();
        for (int i = 0; i < 9; i++) {
            NumberWithBalloon balloon = new NumberWithBalloon();
            balloon.setLayoutX(30);
            balloon.setLayoutY(30 + 30 * i + 10 * i);
            balloon.setText(String.valueOf(10000 - (i * 1000)));
            getChildren().add(balloon);
            numberWithBalloonArrayList.add(balloon);
        }
        for (int i = 0; i < 4; i++) {
            NumberWithBalloon balloon1 = new NumberWithBalloon();
            balloon1.setLayoutX(150);
            balloon1.setLayoutY(200 + (30 + 30 * i + 10 * i));
            balloon1.setText(String.valueOf(100 - (i * 10)));
            getChildren().add(balloon1);
            numberWithBalloonArrayList.add(balloon1);
        }
        for (int i = 0; i < 4; i++) {
            NumberWithBalloon balloon2 = new NumberWithBalloon();
            balloon2.setLayoutX(270);
            balloon2.setLayoutY(200 + (30 + 30 * i + 10 * i));
            balloon2.setText(String.valueOf(60 - (i * 10)));
            getChildren().add(balloon2);
            numberWithBalloonArrayList.add(balloon2);
        }
        for (int i = 0; i < 4; i++) {
            NumberWithBalloon balloon3 = new NumberWithBalloon();
            balloon3.setLayoutX(390);
            balloon3.setLayoutY(200 + (30 + 30 * i + 10 * i));
            balloon3.setText(String.valueOf(25 - (i * 5)));
            getChildren().add(balloon3);
            numberWithBalloonArrayList.add(balloon3);
        }
        for (int i = 0; i < 9; i++) {
            NumberWithBalloon balloon4 = new NumberWithBalloon();
            balloon4.setLayoutX(510);
            balloon4.setLayoutY(30 + 30 * i + 10 * i);
            balloon4.setText(String.valueOf(9 - i));
            getChildren().add(balloon4);
            numberWithBalloonArrayList.add(balloon4);
        }

    }

    public void setTicker() {
        reset = new Button("RESET");
        reset.setLayoutX(260);
        reset.setLayoutY(20);
        reset.setMaxWidth(Double.MAX_VALUE);
        reset.setAlignment(Pos.CENTER);
        reset.setPrefSize(80, 30);
        reset.setStyle("-fx-background-color: #58a5f0;" + "-fx-background-radius: 10 10 0 0;"
                + "-fx-effect: dropshadow( gaussian , #CCCCCC , 0,0,2,2 );");
        reset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!isStarted){
                    createBoard();
                    setTicker();
                    numbersBoard.getScrollPane().setContent(null);
                    numbersBoard.clearPane();
                }
            }
        });

        start = new Button("START");
        start.setLayoutX(260);
        start.setLayoutY(130);
        start.setMaxWidth(Double.MAX_VALUE);
        start.setAlignment(Pos.CENTER);
        start.setPrefSize(80, 30);
        start.setStyle("-fx-background-color: #58a5f0;" + "-fx-background-radius: 0 0 10 10;"
                + "-fx-font: 12 arial;" +"-fx-effect: dropshadow( gaussian , #CCCCCC , 0,0,2,2 , rgba(0,0,0,0.81));");

        ticker = new Label();
        ticker.setLayoutX(260);
        ticker.setLayoutY(50);
        ticker.setPrefSize(80, 80);
        ticker.setStyle("-fx-background-color: #58a5f0;" + "-fx-background-radius: 0 0 0 0;"
                + "-fx-font: 60 arial;" + "-fx-effect: dropshadow( gaussian , #CCCCCC , 0,0,2,2 );");
        ticker.setMaxWidth(Double.MAX_VALUE);
        ticker.setAlignment(Pos.CENTER);
        CountdownTimer countdownTimer = new CountdownTimer();
        countdownTimer.setDuration(0);
        countdownTimer.setCountdownInterface(new CountdownTimer.CountdownInterface() {
            @Override
            public void onTick(int count) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                 //       if (count % 2 == 0) {
                            NumberWithBalloon numberWithBalloon = findNextBalloonWithoutText();
                            if (numberWithBalloon == null) {
                                countdownTimer.cancel();
                                isStarted = false;
                                return;
                            }

                            int number = generateRandomNumber();
                            ticker.setText(String.valueOf(number));
                            numberWithBalloon.setTextOnButton(String.valueOf(number));
//                                String num = numbersBoard.getTicketBoard().findNumber(String.valueOf(number));
//                                if (numberWithBalloon.getText().equals(num)){
//                                    System.out.println(num);
//                                  //  numberWithBalloon.getValue();
//
//                                }
                        if (numbersBoard != null) {
                                numbersBoard.checkTicket(numberWithBalloon);

                            }
                    }
                });
            }

            @Override
            public void onFinished() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<TicketBoard> ticketBoards = new ArrayList<>();
                        for (Ticket ticket : numbersBoard.getArrayList()) {
                            ticketBoards.add(new TicketBoard(ticket));
                        }
                        alert = new MyDialog(ticketBoards);
                        alert.setTitle("Warning!");
                        alert.setHeaderText("You lost! Better luck next time!");
                        alert.showAndWait();
                    }
                });
            }
        });
        getChildren().add(reset);
        getChildren().add(ticker);
        getChildren().add(start);
        start.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (isStarted) {
                    return;
                }

                if (numbersBoard.isTicketCreated()) {
                    countdownTimer.start();
                    isStarted = true;
                } else {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Bingo game");
                    alert.setContentText("");
                    alert.setHeaderText("You have to add at least six numbers!");
                    alert.showAndWait();
                }
            }
        });
    }


    public int generateRandomNumber() {
        Random r = new Random();
        int num = r.nextInt(47) + 1;
        NumberWithBalloon balloon = new NumberWithBalloon();
        balloon.setTextOnButton(String.valueOf(num));
        if (!numberWithBalloonArrayList.contains(balloon)) {
            return num;
        }

        return generateRandomNumber();
    }

    private NumberWithBalloon findNextBalloonWithoutText() {
        for (NumberWithBalloon numberWithBalloon : numberWithBalloonArrayList) {
            if (numberWithBalloon.getText().equals("")) {
                return numberWithBalloon;
            }
        }
        return null;
    }

    public ArrayList<NumberWithBalloon> getNumberWithBalloonArrayList() {
        return this.numberWithBalloonArrayList;
    }

    public ArrayList<String> getTickerList() {
        return this.tickerList;

    }

    public ArrayList<Integer> generateRandomNumbersList(int num) {
        Random r = new Random();
        ArrayList<Integer> numList = new ArrayList<>();
        while (numList.size() < num) {
            int number = r.nextInt(47) + 1;
            if (!numList.contains(number)) {
                numList.add(number);
            }
        }
        Collections.sort(numList);
        return numList;
    }

    public void setNumbersBoard(NumbersBoard numbersBoard) {
        this.numbersBoard = numbersBoard;
    }
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public NumberWithBalloon getNumberWithBalloon() {
        return numberWithBalloon;
    }
}

