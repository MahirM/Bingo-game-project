package sample;

import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Window;

import java.util.ArrayList;

/**
 * Created by Mahir on 03.10.2017..
 */
public class MyDialog extends Dialog {

    private TicketBoard ticketBoard;

    public MyDialog(ArrayList<TicketBoard> tickets) {
        DialogPane dialogPane = new DialogPane();
        VBox vBox = new VBox();
        for (TicketBoard ticket : tickets) {

            vBox.getChildren().add(ticket);
        }
        ScrollPane scrollPane = new ScrollPane(vBox);
        scrollPane.setPrefHeight(500);
        dialogPane.setContent(scrollPane);
        setDialogPane(dialogPane);
        Window window = getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(event -> window.hide());
    }
}
