package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Mahir on 19.09.2017..
 */
public class NumbersBoard extends Pane {
    private ArrayList<String> buttons;
    private ArrayList<MyButton> myButtons;
    private MyButton button;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private NumbersBoard board;
    private ScrollPane scrollPane;
    private NumberWithBalloon numberWithBalloon;
    private ResultBoard resultBoard;
    private ArrayList<Ticket> arrayList = new ArrayList<>();
    private ArrayList<TicketBoard> ticketBoards = new ArrayList<>();
    private TicketBoard ticketBoard;
    private ArrayList<Integer> ticketNumbers = new ArrayList<>();




    public NumbersBoard() {
        setStyle("-fx-background-color: #26C6DA;");
        setPrefSize(400, 400);
        setLayoutX(600);
        setLayoutY(0);
    }

    private void addNumberToTicket(MyButton myButton, Ticket ticket) {
        ticket.addNumber(myButton);
    }

    public void createBoard() {
        myButtons = new ArrayList<>();
        for (int i = 0; i < 6; i++) { //row
            for (int j = 0; j < 8; j++) { //column
                MyButton button = new MyButton();
                button.setLayoutX(10 + (30 * j + j * 10));
                button.setLayoutY(10 + (i * 30 + i * 10));
                button.setText(String.valueOf(myButtons.size() + 1));//Dodaje brojeve na buttone
                button.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
//                        for (Ticket ticket : arrayList) {
//                            if (!ticket.isFinished()) {
//                                addNumberToTicket(button, ticket);
//                                return;
//                            }
//                        }
//                        Ticket ticket = new Ticket();
//                        arrayList.add(ticket);
//                        ticket.setPrefSize(400, 50);
//                        addNumberToTicket(button, ticket);
//                        scrollPane.setContent(createTicketPan());
                            if (ticketNumbers.contains(button.textToNumber())) {
                                ticketNumbers.remove(button.textToNumber());
                                button.setStyle(new MyButton().getStyle());
                            } else {
                                button.setStyle("-fx-background-color: #FF0000;" + "-fx-background-radius: 30px;");
                                ticketNumbers.add(button.textToNumber());
                            }


                    }
                });
                getChildren().add(button);
                myButtons.add(button);
            }
        }
        button1 = new Button("6");
        button1.setPrefSize(60, 20);
        button1.setLayoutX(330);
        button1.setLayoutY(50);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Ticket ticket = new Ticket();
                ticket.setPrefSize(400, 50);
                ticket.createTicket(generateRandomNumbersList(6));
                arrayList.add(ticket);
                scrollPane.setContent(createTicketPan());
            }
        });
        getChildren().add(button1);

        button2 = new Button("7");
        button2.setPrefSize(60, 20);
        button2.setLayoutX(330);
        button2.setLayoutY(100);
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Ticket ticket = new Ticket();
                ticket.setPrefSize(400, 50);
                ticket.createTicket(generateRandomNumbersList(7));
                arrayList.add(ticket);
                scrollPane.setContent(createTicketPan());
            }
        });
        getChildren().add(button2);

        button3 = new Button("8");
        button3.setPrefSize(60, 20);
        button3.setLayoutX(330);
        button3.setLayoutY(150);
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Ticket ticket = new Ticket();
                ticket.setPrefSize(400, 50);
                ticket.createTicket(generateRandomNumbersList(8));
                arrayList.add(ticket);
               scrollPane.setContent(createTicketPan());
            }
        });
        getChildren().add(button3);

        button4 = new Button("Create");
        button4.setPrefSize(60, 20);
        button4.setLayoutX(330);
        button4.setLayoutY(200);
        button4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Ticket ticket = new Ticket();
                ticket.setPrefSize(400, 50);
                if (ticketNumbers.size() >= 6 && ticketNumbers.size() <=8 ){
                    ticket.createTicket(ticketNumbers);
                    arrayList.add(ticket);
                    scrollPane.setContent(createTicketPan());
                    for (MyButton myButton : myButtons) {
                        myButton.setStyle(new MyButton().getStyle());
                    }
                    ticketNumbers.clear();
                    }else {
                    for (MyButton myButton : myButtons) {
                        myButton.setStyle(new MyButton().getStyle());
                    }
                    ticketNumbers.clear();
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setContentText("You have to add six, seven or eight numbers to start a game!");
                    alert.setTitle("Bingo Game");
                    alert.setHeaderText("We are sorry!");
                    alert.showAndWait();


                }
            }
        });
        getChildren().add(button4);

        scrollPane = new ScrollPane();
        scrollPane.setPrefSize(400, 150);
        scrollPane.setLayoutX(0);
        scrollPane.setLayoutY(250);
        getChildren().add(scrollPane);




    }

    private Pane createTicketPan() {
        VBox pane = new VBox();
        int i = 0;
        for (Ticket ticket : arrayList) {
            pane.getChildren().add(ticket);
            i++;
        }
        return pane;
    }

    public ArrayList<Integer> generateRandomNumbersList(int num) {
        Random r = new Random();
        ArrayList<Integer> numList = new ArrayList<>();
        while (numList.size() < num) {
            int number = r.nextInt(47) + 1;
            if (!numList.contains(number)) {
                numList.add(number);
            }
        }
        Collections.sort(numList);
        return numList;
    }

    public void checkTicket(NumberWithBalloon numberWithBalloon) {
        for (Ticket ticket : arrayList) {
            ticket.findNumber(numberWithBalloon);
        }
    }
    public Boolean isTicketCreated() {
        return arrayList.size() > 0;
    }

    public Boolean isButtonTicketCreated() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText("You have to add more at least six numbers!");
        alert.showAndWait();

        return ticketNumbers.size() < 6;
    }
    public boolean checkIsWin() {
        //return ticketBoard.checkIsWin();
        return true;
    }
    public String getMyLastNumber() {
       // return ticketBoard.getMyLastNumber();
        return "";
    }
    public NumberWithBalloon getNumberWithBalloon() {
        return numberWithBalloon;
    }

    public ResultBoard getResultBoard() {
        return resultBoard;
    }

    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    public void printMyTickets() {
        for (Ticket ticket : arrayList) {
            System.out.println(ticket.getMyLastNumber().getText() + "  " + ticket.getMyLastNumber().getValue());
        }
    }

    public void clearPane() {
        arrayList.clear();
    }

    public ArrayList<Ticket> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Ticket> arrayList) {
        this.arrayList = arrayList;
    }

    public ArrayList<TicketBoard> getTicketBoards() {
        return ticketBoards;
    }

    public TicketBoard getTicketBoard() {
        return ticketBoard;
    }
}

