package sample;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Mahir on 20.09.2017..
 */
public class NumberWithBalloon extends Pane {

    private Label label;
    private MyButton button;

    public NumberWithBalloon(){
        setPrefSize(100, 30);
        button = new MyButton();
        button.setStyle("-fx-background-color: #58a5f0;" + "-fx-background-radius: 30px;");
        getChildren().add(button);

        label = new Label("Test");
        label.setLayoutX(40);
        label.setPrefHeight(30);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setAlignment(Pos.CENTER);
        label.setFont(Font.font("Helvetica", 20));
        getChildren().add(label);
    }

    public NumberWithBalloon(NumberWithBalloon numberWithBalloon) {
        super(numberWithBalloon);
    }

    public void setText(String text){
        label.setText(text);
    }
    public void setTextOnButton(String text){
        button.setText(text);
    }
    public String getText(){
        return button.getText();
    }
    public int getValue() {
       return Integer.parseInt(label.getText());
    }

    public void setNumber(int number){
        ArrayList<Integer> numList = new ArrayList<>();
        Random random = new Random();
        while (numList.size() < number){
            Integer num = random.nextInt(9);
            if (!numList.contains(num)){
                 numList.add(num);
                button.setText(num.toString());
            }
        }
        return;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NumberWithBalloon balloon = (NumberWithBalloon) o;

        return button != null ? button.getText().equals(balloon.button.getText()) : balloon.button.getText() == null;
    }

    @Override
    public int hashCode() {
        return label != null ? label.hashCode() : 0;
    }
}


