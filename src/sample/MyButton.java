package sample;

import javafx.geometry.Pos;
import javafx.scene.control.Button;

import java.util.Objects;

/**
 * Created by Mahir on 19.09.2017..
 */
public class MyButton extends Button implements Comparable {

    private boolean isChecked = false;

    public MyButton() {
        setStyle("-fx-background-color: #006064");
        setStyle("-fx-background-radius: 30px;");
        setPrefSize(30, 30);
        setMaxWidth(Double.MAX_VALUE);
        setAlignment(Pos.CENTER);
    }

    public MyButton(String text) {
        setStyle("-fx-background-color: #006064");
        setStyle("-fx-background-radius: 30px;");
        setPrefSize(30, 30);
        setMaxWidth(Double.MAX_VALUE);
        setAlignment(Pos.CENTER);
        setText(text);
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Integer textToNumber() {
        try {
            return Integer.parseInt(this.getText());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    @Override
    public int compareTo(Object o) {
        MyButton myButton = (MyButton) o;
        if (this.textToNumber() > myButton.textToNumber()) {
            return 1;
        }
        if (Integer.parseInt(this.getText()) < Integer.parseInt(myButton.getText())) {
            return -1;
        }
        return 0;
    }
}

