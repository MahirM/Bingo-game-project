package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;


import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Mahir on 20.09.2017..
 */
public class Ticket extends Pane {

    private ArrayList<MyButton> myButtons = new ArrayList<>();
    private ArrayList<Ticket> ticketArrayList = new ArrayList<>();
    private MyButton button;
    private NumberWithBalloon numberWithBalloon;
    private NumbersBoard numbersBoard;

    private ArrayList<Integer> numList = new ArrayList<>();

    private ResultBoard resultBoard;
    private NumberWithBalloon myLastNumber;
    private Label label;
    private boolean isFinished = false;


    private TableView<Integer> table;
    TableColumn ticketList = new TableColumn();
    TableColumn numberOfHits = new TableColumn();

    public Ticket() {
        setPrefSize(400, 200);
        setStyle("-fx-background-color: #007c91;");
        setLayoutY(400);
        setLayoutX(600);
    }

    public Ticket (Ticket ticket) {
         super(ticket);
    }

    public void createTicket(ArrayList<Integer> numList) {
        isFinished = true;
        this.numList = numList;
        getChildren().removeAll(myButtons);
        myButtons.clear();
        int startX = (400 - (40 * numList.size())) / 2;
        for (int row = 0; row < numList.size(); row++) {
            button = new MyButton();
            button.setLayoutY(10);
            button.setLayoutX(startX + (30 * row + row * 10));
            button.setText(String.valueOf(numList.get(row)));
            getChildren().add(button);
            myButtons.add(button);
//            createListView(numList);

        }
    }
    public void createListView(ArrayList<Integer> viewList){
        ObservableList<ArrayList<Integer>> data = FXCollections.observableArrayList();
        ListView<ArrayList<Integer>> table = new ListView<>();
        data.add(viewList);
        table.setLayoutX(10);
        table.setLayoutY(50);
        table.setPrefWidth(380);
        table.setPrefHeight(100);
        table.setItems(data);
        getChildren().addAll(table);
    }
    public void setLabelText(){
        myButtons.toString();
    }
    public void removeAllTickets(){
        getChildren().removeAll(myButtons);
    }

    public int findNextTicketNumber() {
        int n = myButtons.size();
        NumberWithBalloon balloon = new NumberWithBalloon();
            if (!myButtons.contains(balloon)) {
                return n;
        }
        return findNextTicketNumber();
    }

    private int generateRandomNum(){
        int num = resultBoard.generateRandomNumber();
        if (myButtons.contains(num))
        System.out.println(num);
        return num;
    }

    public String findNumber(NumberWithBalloon numberWithBalloon) {
       for (MyButton myButton : myButtons) {
           if (myButton.getText().equals(numberWithBalloon.getText())) {
               myButton.setStyle("-fx-background-color: #FF0000;" + "-fx-background-radius: 30px;");
               myButton.setChecked(true);
               myLastNumber = numberWithBalloon;
           }
       }
        return numberWithBalloon.getText();
    }
    public boolean isTicketCreated() {
        return myButtons.size() >= 6;
    }
//    public boolean isTicketSmallerThanSix() {
//        return myButtons.size() == 6;
//    }

    public void addNumber(MyButton myButton) {
        if (myButtons.size() == 8 || checkIfButtonExist(myButton)) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("Number already added to ticket!");
                    alert.showAndWait();
                    isFinished = true;
            return;
        }

        MyButton testButton = new MyButton();
        testButton.setText(myButton.getText());
       for (MyButton myButton1 : myButtons) {
           getChildren().remove(myButton1);
       }
        numList.add(myButton.textToNumber());
        myButtons.add(testButton);
        Collections.sort(myButtons);
        int startX = (400 - (40 * myButtons.size())) / 2;
        for (int row = 0; row < myButtons.size(); row++) {
            myButtons.get(row).setLayoutY(10);
            myButtons.get(row).setLayoutX(startX + (30 * row + row * 10));
            getChildren().add(myButtons.get(row));
        }
    }
    private boolean checkIfButtonExist(MyButton newButton){
        for (MyButton myButton : myButtons){
            if (myButton.getText().equals(newButton.getText())){
              return true;
            }
        }
        return false;
    }
    public void restart(){
        getChildren().removeAll(myButtons);
        myButtons.clear();
    }

    public ArrayList<Integer> getNumList() {
        return numList;
    }

    public void setNumList(ArrayList<Integer> numList) {
        this.numList = numList;
    }

    public boolean checkIsWin() {
        for (MyButton button : myButtons) {
            if (!button.isChecked()) {
                return  false;
            }
        }
        return true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public NumberWithBalloon getMyLastNumber() {
        return myLastNumber;
    }
    public ResultBoard getResultBoard() {
        return resultBoard;
    }
    public void setText(String num){
        label.setText(String.valueOf(num));
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public ArrayList<MyButton> getMyButtons() {
        return myButtons;
    }
}






