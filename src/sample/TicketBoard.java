package sample;

import javafx.scene.layout.Pane;

import java.util.ArrayList;

/**
 * Created by Mahir on 04.10.2017..
 */
public class TicketBoard extends Pane {
    private Ticket ticket;
    private NumberWithBalloon number;
    private ArrayList<TicketBoard> ticketBoards = new ArrayList<>();

    public TicketBoard(Ticket ticket) {
        setPrefSize(ticket.getPrefWidth() + 50 + 20, 150);
        createTicketBoard(ticket);
    }

        public void createTicketBoard(Ticket ticket1){
        Ticket ticket = new Ticket();
        ticket.createTicket(ticket1.getNumList());
        ticket.setLayoutY(10);
        ticket.setLayoutX(10);
        for (MyButton myButton : ticket1.getMyButtons()){
            for (MyButton myButton1 : ticket.getMyButtons()){
                if (myButton1.getText().equals(myButton.getText())){
                    myButton1.setStyle(myButton.getStyle());
                }
            }
        }
        ticket.setPrefSize(ticket1.getPrefWidth(), ticket1.getPrefHeight());

        getChildren().add(ticket);

        NumberWithBalloon number = new NumberWithBalloon();
        number.setLayoutY(10);
        number.setLayoutX(ticket1.getPrefWidth() + 20);
        number.setPrefSize(50, 50);
        number.setTextOnButton(ticket1.getMyLastNumber().getText());
        number.setText("");
        getChildren().add(number);
    }

    public ArrayList<TicketBoard> getTicketBoards() {
        return ticketBoards;
    }
}
