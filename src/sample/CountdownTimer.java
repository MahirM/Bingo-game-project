package sample;

import javafx.scene.control.Alert;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Mahir on 18.09.2017..
 */
public class CountdownTimer {

    private int duration;
    private int count = 0;
    private CountdownInterface countdownInterface;
    private boolean shouldStop = false;
    private boolean isCanceled = false;


    public CountdownTimer() {

    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void start() {
        isCanceled = false;
        shouldStop = false;
        count = -1;
        tick();
    }

    public void cancel() {
        isCanceled = true;
    }

    public void stop() {
        shouldStop = true;
    }

    private void tick() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (duration != 0) {
                    if (duration <= count * 1000 || shouldStop || isCanceled) {
                        if (countdownInterface != null) {
                            countdownInterface.onFinished();
                        }

                        return;
                    }
                } else if (isCanceled) {
                    if (countdownInterface != null) {
                        countdownInterface.onFinished();
                    }
                    return;
                }


                if (countdownInterface != null) {
                    countdownInterface.onTick(count + 1);
                }

                tick();
                count++;
            }
        };
        new Timer().schedule(timerTask, 1000);
    }

    public void setCountdownInterface(CountdownInterface countdownInterface) {
        this.countdownInterface = countdownInterface;
    }

    public void resume() {
        shouldStop = false;
        tick();
    }

    protected interface CountdownInterface {
        void onTick(int count);
        void onFinished();
    }
}
