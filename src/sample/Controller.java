package sample;


import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public NumbersBoard numbersBoard;
    public ResultBoard resultBoard;
    public CountdownTimer countdownTimer;
    public NumberWithBalloon balloon;
    public Ticket ticket;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        numbersBoard.createBoard();
        resultBoard.setNumbersBoard(numbersBoard);

    }
}
